export const item = {
    success: 'ITEM_LOAD_SUCCESS',
    error: 'ITEM_LOAD_ERROR'
}

export const category = {
    success: 'CATEGORY_LOAD_SUCCESS',
    error: 'CATEGORY_LOAD_ERROR'
}

export const itemId = {
    success: 'ITEM_ID_LOAD_SUCCESS',
    error: 'ITEM_ID_LOAD_ERROR'
}

export const korzina = {
    success: 'ITEM_ADD_SUCCESS',
    error: 'ITEM_ADD_ERROR',
    allsuccess: 'ITEM_ALL_LOAD_SUCCESS',
    allerror: 'ITEM_ALL_LOAD_ERROR',
    secondsuccess: 'ITEM_ADD_SECOD_SUCCESS',
    seconderror: 'ITEM_ADD_SECOND_ERROR'
}

export const lang = {
    success: 'LANG_CHANGED_SUCCESS',
    error: 'LANG_CHANGED_ERROR'
}

export const deleteItem = {
    success: 'DELETE_ITEM_SUCCESS',
    error: 'DELETE_ITEM_ERROR'
}

export const update={
    success:'UPDATE_ITEM_SUCCESS',
    error:'UPDATE_ITEM_ERROR'
}
