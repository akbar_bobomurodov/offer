import { item, category, itemId, korzina, lang,deleteItem } from '../constant/index';
import { take, call, put, all, takeLatest } from 'redux-saga/effects';
import { loadItem, loadCategory, loadItemById, addKorzina, allKorzina, addSecondKorzina } from '../api/api.js';

// <------------- ITEM ------------->

function* workerLoadItem(action) {
    try {
        const data = yield call(loadItem, action.payload);
        yield put({ type: item.success, payload: data })
    } catch (err) {
        yield put({ type: item.error, payload: 'Error' })
    }
}

function* watcherLoadItem() {
    while (true) {
        const action = yield take(item.success)
        yield call(workerLoadItem, action);
    }
}

function* workerLoadItemById(payload) {
    try {
        const data = yield call(loadItemById, payload);
        yield put({ type: itemId.success, payload: data })
    } catch (err) {
        put({ type: itemId.error, paylaod: 'Error id' })
    }
}

function* watcherLoadItemById() {
    while (true) {
        const action = yield take(itemId.success);
        yield call(workerLoadItemById, action.payload)
    }
}

// <----------- CATEGORY -------------------->

function* workerLoadCategory() {
    try {
        const data = yield call(loadCategory);
        yield put({ type: category.success, payload: data })
    } catch (err) {
        yield put({ type: category.error, payload: 'Error' })
    }
}

function* watcherLoadCategory() {
    while (true) {
        const action = yield take(category.success)
        yield call(workerLoadCategory)
    }
}

// <--------------- KORZINA ----------------->
function* workerAddKorzina(doc) {
    try {
        const data = yield call(addKorzina);
        put({ type: korzina.success, payload: data })
    } catch (err) {
        yield put({ type: korzina.error, payload: 'Error' })
    }
}

function* watcherAddKorzina() {
    yield takeLatest(korzina.success, workerAddKorzina);
}

function* workerAddSecondKorzina(doc) {
    try {
        const data = yield call(addSecondKorzina, doc);
        put({ type: korzina.secondsuccess, payload: data })
    } catch (err) {
        yield put({ type: korzina.error, payload: 'Error' })
    }
}

function* watcherAddSecondKorzina(doc) {
    while (true) {
        const action = yield take(korzina.secondsuccess, doc)
        yield call(workerAddSecondKorzina, action)
    }
}


function* workerAllKorzina() {
    try {
        const data = yield call(allKorzina);
        yield put({ type: korzina.allsuccess, payload: data })
    } catch (err) {
        yield put({ type: korzina.allerror, payload: 'Error all orders' })
    }
}

function* watcherAllKorzina() {
    while (true) {
        const action = yield take(korzina.allsuccess);
        yield call(workerAllKorzina);
    }
}

// <------------- LANG ---------------->

function* workerChangeLanguage(payload) {
    try {
        yield put({ type: lang.success, payload: payload })
    } catch (error) {
        throw new Error('error')
    }
}
function* watcherChangeLanguage() {
    while (true) {
        const action = yield take(lang.success);
        call(workerChangeLanguage, action.paylaod)
    }
}

// <-------------- DELETE ITEM -------->
function* workerDeleteItem() {
    try {
        const data=yield call(deleteItem)
        yield put({ type: deleteItem.success, payload: data})
    } catch (error) {
        throw new Error('error')
    }
}
function* watcherDeleteItem() {
    while (true) {
        const action = yield take(deleteItem.success);
        call(workerDeleteItem, action.paylaod)
    }
}


export default function* rootSaga() {
    yield all([
        watcherLoadItem(),
        watcherLoadCategory(),
        watcherLoadItemById(),
        watcherAddKorzina(),
        watcherAllKorzina(),
        watcherChangeLanguage(),
        watcherAddSecondKorzina(),
        watcherDeleteItem()
    ])
}
