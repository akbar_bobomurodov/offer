import React from 'react';
import _ from 'lodash';
import Dashboard from '../../component/dashboard';
import Pagination from 'react-js-pagination';
import { Spin } from 'antd';
import { connect } from 'react-redux';
import { loadItem } from '../../action/index';
import ItemWidget from './item_widget';
import '../css/item.css';
class Item extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            currentPage: 1,
            postsPages: 9
        }
    }
    componentDidUpdate(prevProps, prevState) {
        console.log('item page')
        console.log(prevState.currentPage)
        console.log(this.state.currentPage)
    }
    componentDidMount() {
        this.props.dispatch(loadItem(this.state.postsPages, this.state.currentPage))
    }
    handlePageChange = async (pageNumber) => {
        console.log(pageNumber)
        await this.setState({
            currentPage: pageNumber
        })
        console.log(this.state.currentPage)
        this.props.dispatch(loadItem(this.state.postsPages, this.state.currentPage))
    }
    render() {
        let data = _.get(this.props.items, 'item.data.data');
        return (
            <Dashboard>
                {
                    !data ? <div className="spin"><Spin size="large" /></div> : <ItemWidget data={data} />
                }
                <div className="parent_pagination">
                    <Pagination
                        activePage={this.state.currentPage}
                        itemsCountPerPage={this.postsPages}
                        totalItemsCount={_.get(this.props.items, 'item.data.total')}
                        pageRangeDisplayed={3}
                        onChange={this.handlePageChange}
                    />
                </div>
            </Dashboard>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        items: state.items
    }
}

export default connect(mapStateToProps)(Item);
