import React from 'react';
import { Card } from 'antd';
import { Link } from 'react-router-dom';
import '../css/category.css';
const CategoryWidget = (props) => {
    return (
        <div style={{ display: 'flex', justifyContent: 'flex-start', flexWrap: 'wrap',perspective:'1000px' }}>
            {
                props.data.map((item, id) => (
                    <Link id="category_id" key={id} to={`/dashboard/category/${item._id}`} style={{ margin: '20px' }}>
                        <Card style={{ width: 200, height: 100 }}>
                            <h2>{item.name}</h2>
                        </Card>
                    </Link>
                ))
            }
        </div>
    )
}

export default CategoryWidget;