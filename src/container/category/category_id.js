import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import Pagination from 'react-js-pagination';
import Dashboard from '../../component/dashboard';
import {connect} from 'react-redux';
import {loadItemId} from '../../action/index';
import {Spin} from 'antd';
import CategoryIdWidget from './category_id_widget';


class CategoryId extends React.Component{
    state={
        currentPage:1,
        postsPages:9
    }
    componentDidMount(){
        let id=this.props.match.params.id;
        this.props.dispatch(loadItemId(this.state.postsPages,this.state.currentPage, id));
    }
    handlePageChange = (pageNumber) => {
        let id=this.props.match.params.id;
        this.setState({
            currentPage:pageNumber
        })
        this.props.dispatch(loadItemId(this.state.postsPages,this.state.currentPage, id))
      }
    render(){
    let data=_.get(this.props.items, 'itemId.data.data');
        return(
            <Dashboard>
              {
                 !data ? <div className="spin"><Spin size="large"/></div>:<CategoryIdWidget data={data} />
              }
              <div className="parent_pagination">
              <Pagination
                  activePage={this.state.currentPage}
                  itemsCountPerPage={this.postsPages}
                  totalItemsCount={_.get(this.props.items, 'itemId.data.total' )}
                  pageRangeDisplayed={3}
                  onChange={this.handlePageChange}
              />
          </div>
            </Dashboard>
        )
    }
}

const mapStateToProps=(state)=>{
    return{
       items:state.items
    }
}

export default connect(mapStateToProps)(CategoryId);