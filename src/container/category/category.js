import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Spin } from 'antd';
import Dashboard from '../../component/dashboard';
import CategoryWidget from './category_widget.js'
import { loadCategory } from '../../action/index';
class Category extends React.Component {

    componentDidMount() {
        this.props.dispatch(loadCategory())
    }

    render() {
        let data = _.get(this.props.categories, 'category.data')
        return (
            <Dashboard>
                {
                    !data ? <div className="spin"><Spin size="large" /></div> : <CategoryWidget data={data} />
                }

            </Dashboard>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        categories: state.categories
    }
}


export default connect(mapStateToProps)(Category);