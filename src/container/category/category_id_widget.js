import React, { useState, useEffect } from 'react';
import { Rate } from 'antd';
import { connect } from 'react-redux';
import { addKorzina, addSecondKorzina } from '../../action/index';
import Korzina from '../../korzina/korzina';
import '../css/item.css';
import '../../component/css/dashboard.css';
import { Button } from 'antd';
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
const CategoryIdWidget = (props) => {
    let [items, setItems] = useState(props.data);
    let [count, setCount] = useState(0)
    const [foodName, setFoodName] = useState('');
    let [lang, setLang] = useState('UZ');
    const [viewInput, setViewInput] = useState(true);

    useEffect(() => {

    }, [lang])
    let hideInput = () => {
        let input = document.getElementsByName('search')[0];
        setViewInput(!viewInput);
        if (viewInput === true) {
            input.style.display = 'block'
        } else {
            input.style.display = 'none'
        }
    }
    let searchFood = async (e) => {
        if (e.target.value.split('').length > 0) {
            setFoodName(e.target.value)
            let newItems = await items.filter(item => {
                let a = item.name.toLowerCase();
                return a.search(`${e.target.value.trim()}`) > -1
            })
            setItems(newItems)
        } else {
            setFoodName(null)
            setItems(props.data)
        }
    }
    let add = (e) => {
        allCansel();
        let order = readCookie('order_id');
        if (order == null) {
            props.dispatch(addKorzina());
        }
        let image = document.getElementsByName(e.target.id)[0];
        image.style.height = '0px';
        let counter = document.getElementsByName(e.target.id)[1];
        counter.classList.add('view');
        counter.classList.add(e.target.id)
    }
    let getValue = (e) => {
        let div = document.getElementsByName(e.target.name)[11];
        if(isNaN(Number(div.innerText))){
            div.innerText=null
        }
            let all = Number(div.innerText) + Number(e.target.value)
            div.innerHTML = all
    }
    let allCansel = () => {
        let k=document.getElementsByClassName('k');
        let d = document.getElementsByClassName('numbers');
        for (let i = 0; i < d.length; i++) {
            if (d[i].className !== 'numbers') {
                d[i].classList.remove('view');
                let img = document.getElementsByTagName('img');
                k[i].innerText=0;
                for (let i = 0; i < img.length; i++) {
                    img[i].style.height = '70%'
                }
            }
        }
    }
    let cansel = (e) => {
        let counter = document.getElementsByName(e.target.id)[1];
        counter.classList.remove('view')
        document.getElementsByName(e.target.id)[0].style.height = '70%'
    }
    let save = async (e) => {
        let div = document.getElementsByName(e.target.name)[11]
        let element = items.filter(item => { return item._id == e.target.name });
        if(Number(div.innerText)>0){
            props.dispatch(addSecondKorzina(element, div));
            setTimeout(() => {
                allCansel();
            }, 500)
        }else{
            div.innerText='0 ta buyurish mumkin emas'
        }
    }
    return (
        <div className="item">
            <nav style={{ position: 'fixed', top: '4px', width: '80%', zIndex: 2 }}>
                <header style={{ background: 'white', padding: 0, marginTop: '6px', width: '80%', margin: 'auto', height: '40px' }}>
                    <div>
                        <input name="search" type="text" placeholder="Search food ..." value={foodName} onChange={searchFood} />
                    </div>
                    <div>
                        <img style={{ width: '20px', height: '20px', marginRight: '20px' }}
                            onClick={hideInput}
                            src="https://cdn.icon-icons.com/icons2/610/PNG/512/magnifier_icon-icons.com_56375.png" alt="" />
                    </div>
                </header>
            </nav>
            {
                items.map((item, id) => (
                    <div key={id} style={{ position: 'relative' }}>
                        <img
                            className="image"
                            name={item._id}
                            src={item.representation.length > 7 ?
                                `${item.representation}`
                                : 'https://images.unsplash.com/photo-1460306855393-0410f61241c7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'} alt={item._id} />
                        <div className="numbers" name={item._id}>
                            <input value="1" name={item._id} onClick={getValue} /><input value="2" name={item._id} onClick={getValue} /><input value="3" name={item._id} onClick={getValue} />
                            <input value="4" name={item._id} onClick={getValue} /><input value="5" name={item._id} onClick={getValue} /><input value="6" name={item._id} onClick={getValue} />
                            <input value="7" name={item._id} onClick={getValue} /><input value="8" name={item._id} onClick={getValue} /><input value="9" name={item._id} onClick={getValue} />
                            <div name={item._id} className="k">
                                0
                            </div>
                            <button onClick={save} name={item._id}>Save</button><button onClick={cansel} id={item._id}>Cansel</button>
                        </div>
                        <div className="mini_item">
                            <div>
                                <h3>{item.name}</h3>
                                <div id="rate">
                                    <Rate disabled count={3} defaultValue={2} />
                                </div>
                                <div id="price">
                                    <p>{item.price}</p>
                                </div>
                                <p onClick={add} id={item._id} className="add">Add Card</p>
                            </div>
                        </div>
                    
                    </div>
                ))
            }
        </div>
    )
}
const mapStateToProps = (state) => {
    return {
        korzinaes: state.korzinaes
    }
}
export default connect(mapStateToProps)(CategoryIdWidget);









