import axios from 'axios';
import _ from 'lodash';
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

// <-------------- ITEM ----------------------->

export const loadItem = async (payload) => {
    const token = readCookie('query');
    const response = await axios({
        method: 'POST',
        url: `https://api.invan.uz/goods/sales/searching/${payload.postsPages}/${payload.currentPage}`,
        headers: {
            'Content-Type': 'application/json',
            'Accept-Version': '1.0.0',
            'Accept-User': 'QRCode'
        },
        data: {
            table_id: token
        }
    })
    return response.data;
}

export const loadItemById = async (payload) => {
    const token = readCookie('query');
    const response = await axios({
        method: 'POST',
        url: `https://api.invan.uz/goods/sales/searching/${payload.postsPages}/${payload.currentPage}`,
        headers: {
            'Content-Type': 'application/json',
            'Accept-Version': '1.0.0',
            'Accept-User': 'QRCode'
        },
        data: {
            table_id: token,
            category_id: payload.id
        }
    })

    console.log(payload.id);

    console.log(response.data)

    return response.data
}

// <-------------- CATEGORY ----------------->

export const loadCategory = async () => {
    const token = readCookie('query');
    const response = await axios({
        method: 'POST',
        url: `https://api.invan.uz/goods/category/search`,
        headers: {
            'Content-Type': 'application/json',
            'Accept-Version': '1.0.0',
            'Accept-User': 'QRCode'
        },
        data: {
            table_id: token
        }
    })
    return response.data;
}

// <----------- KORZINA --------------->

export const addKorzina = async () => {
    const token = readCookie('query');
    const response = await axios({
        method: 'POST',
        url: `https://api.invan.uz/orders/create`,
        headers: {
            'Content-Type': 'application/json',
            'Accept-Version': '1.0.0',
            'Accept-User': 'QRCode'
        },
        data: {
            table_id: token
        }
    })
    let id = _.get(response.data, 'data._id')
    console.log(id)
    if (response.data) {
        document.cookie = 'order_id=' + encodeURIComponent(id)
    }
    return response.data;
}

export const addSecondKorzina = async (data) => {
    let a = _.get(data.payload, 'data[0]')
    const token = readCookie('query');
    const order_id = readCookie('order_id')
    console.log(data.payload.div.innerText)
    const response = await axios({
        method: 'POST',
        url: `https://api.invan.uz/order_items/create`,
        headers: {
            'Content-Type': 'application/json',
            'Accept-Version': '1.0.0',
            'Accept-User': 'QRCode'
        },
        data: {
            table_id: token,
            product_id: a._id,
            order_id: order_id,
            product_name: a.name,
            price: a.price,
            cost: a.cost,
            count: Number(data.payload.div.innerText)
        }
    })
    console.log(response.data)
    return response.data;
}

export const allKorzina = async () => {
    const token = readCookie('query');
    const order_id = readCookie('order_id')
    const response = await axios({
        method: 'POST',
        url: 'https://api.invan.uz/order_items/search',
        headers: {
            'Accept-Version': '1.0.0',
            'Accept-User': 'QRCode',
            'Content-Type': 'application/json'
        },
        data: {
            table_id: token,
            order_id: order_id
        }
    })

    console.log(response.data)
    return response.data
}

// <----------------- DELETE ITEM ----------->

export const deleteItem = async () => {
    const order_id = readCookie('order_id');
    const response = await axios({
        method: 'DELETE',
        url: `https://api.invan.uz/order_items/delete`,
        headers: {
            'Accept-Version': '1.0.0',
            'Accept-User': 'QRCode'
        }
    })
    console.log(response.data)
    return response.data
}