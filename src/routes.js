import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Category from './container/category/category';
import Example from './query';
import Item from './container/item/item';
import CategoryId from './container/category/category_id';
import Korzina from './korzina/korzina';

class Routes extends React.Component {

    render() {
        return (
            <div>
                <Router>
                    <Switch>
                        <Route exact path="/" component={Example} />
                        <Route exact path="/dashboard" component={Item} />
                        <Route exact path="/dashboard/category" component={Category} />
                        <Route exact path="/dashboard/korzina" component={Korzina} />
                        <Route path="/dashboard/category/:id" component={CategoryId} />
                    </Switch>
                </Router>

            </div>
        )
    }
}

export default Routes;