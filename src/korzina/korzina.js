import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import {Spin} from 'antd';
import { allKorzina } from '../action/index';
import Dashboard from '../component/dashboard';
import KorzinaWidget from './korzina_widget';
import './css/korzina.css';

class Korzina extends React.Component {

    componentDidMount() {
        this.props.dispatch(allKorzina())
    }

    render() {
       let data=_.get(this.props.korzinaes, 'items.data');
       console.log(data)
        return (
            <Dashboard>
                {
                    !data ?<div className="spin"><Spin size="large"/></div>:<KorzinaWidget data={data}/>
                }
            </Dashboard>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        korzinaes: state.korzinaes
    }
}

export default connect(mapStateToProps)(Korzina);