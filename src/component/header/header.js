import React from 'react';
import './css/header.css';

class Header extends React.Component {

    render() {
        return (
            <header>
                <div>

                </div>
                <div>
                    <select>
                        <option value="UZ">UZ</option>
                        <option value="RU">RU</option>
                        <option value="EN">EN</option>
                    </select>
                    <img style={{
                        width:'30px',
                        height:'30px',
                        marginRight:'20px'
                    }} src="https://cdn.icon-icons.com/icons2/610/PNG/512/magnifier_icon-icons.com_56375.png" alt=""/>
                </div>
            </header>
        )
    }
}

export default Header;
