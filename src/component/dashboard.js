import React from 'react';
import { Layout, Menu, Icon, Badge } from 'antd';
import { Link } from 'react-router-dom';
import _ from 'lodash'
import { connect } from 'react-redux';
import { changeLang, allKorzina } from '../action/index';
import { texts } from '../lang';
import GetToken from '../hoc/token';
import './css/dashboard.css';
const { Header, Content, Footer, Sider } = Layout;

let token = new GetToken();
class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lang: 'UZ'
        }
    }
    componentDidMount() {
        this.props.dispatch(allKorzina())
        if (!token.token()) {
            this.setState({
                lang: 'UZ'
            })
        } else {
            this.setState({
                lang: token.token()
            })
        }
        console.log(texts[this.state.lang].category)
    }
    changeValue = async (e) => {
        console.log(e.target.value)
        document.cookie = 'lang=' + encodeURIComponent(e.target.value);
        await this.setState({
            lang: e.target.value
        })
        await this.props.dispatch(changeLang(this.state.lang))
        console.log(this.state.lang)
    }

    render() {
        let a = _.get(this.props.korzinaes, 'items.data');
        console.log(a);
        return (
            <Layout>
                <Sider
                    breakpoint="lg"
                    collapsedWidth="0"
                    onBreakpoint={broken => {
                        console.log(broken);
                    }}
                    onCollapse={(collapsed, type) => {
                        console.log(collapsed, type);
                    }}
                >
                    <div className="logo" style={{ width: '100%', height: '40px', background: '#999', margin: '30px 0 10px 0 ' }} />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
                        <Menu.Item key="1">
                            <Icon type="user" />
                            <span className="nav-text"><Link to="/dashboard/category">{texts[this.state.lang].category}</Link></span>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Icon type="video-camera" />
                            <span className="nav-text"><Link to="/dashboard">{texts[this.state.lang].items}</Link></span>
                        </Menu.Item>
                        <Menu.Item key="3">
                            <Icon type="upload" />
                            <span className="nav-text">{texts[this.state.lang].orders}</span>
                        </Menu.Item>
                        <Menu.Item key="4">
                            <Icon type="user" />
                            <span className="nav-text">{texts[this.state.lang].category}</span>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <div style={{ position: 'fixed', zIndex: '2', top: '10px', right: '50px' }}>
                    <Badge count={a === [] || a == undefined ? 0 : a.length}>
                        <a href="/dashboard/korzina" className="head-example">d</a>
                    </Badge>
                    <select onChange={this.changeValue} value={this.state.lang}>
                        <option value="UZ">UZ</option>
                        <option value="RU">RU</option>
                    </select></div>
                <Layout style={{ position: 'relative' }}>
                    <Content style={{ margin: '40px 16px 0' }}>
                        {this.props.children}
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
                </Layout>
            </Layout>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        langs: state.langs,
        korzinaes: state.korzinaes
    }
}

export default connect(mapStateToProps)(Dashboard);

