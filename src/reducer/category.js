import {category} from '../constant/index';

export default function(state={},{type, payload}){
    switch(type){
        case category.success:
            return{
                ...state,
                category:payload
            }
        default:
            return{
                ...state
            }
    }
}