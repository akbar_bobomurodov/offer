import {combineReducers} from 'redux';
import rootReducer from './item';
import categoryReducer from './category';
import korzinaReducer from './korzina';
import langReducer from './lang';
import deleteReducer from './delets';
export default combineReducers({
    items:rootReducer,
    categories:categoryReducer,
    korzinaes:korzinaReducer,
    langs:langReducer,
    delets:deleteReducer
})
