import { korzina } from '../constant/index';
export default function (initialState = {}, { type, payload }) {
    switch (type) {
        case korzina.success:
            return {
                ...initialState,
                item: payload
            }
        case korzina.allsuccess:
            return {
                ...initialState,
                items: payload
            }
        case korzina.secondsuccess:
            return {
                ...initialState,
                secondItem: payload
            }
        default:
            return {
                ...initialState
            }
    }
}