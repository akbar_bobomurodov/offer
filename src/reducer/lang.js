import {lang} from '../constant/index';
export default function(initialState={},{type, payload}){
    switch(type){
        case lang.success:
            return{
                ...initialState,
                lang:payload
            }
        default:
            return{
                ...initialState
            }
    }
}