import {deleteItem} from '../constant/index';
export default function(initialState={},{type, payload}){
    switch(type){
        case deleteItem.success:
            return{
                ...initialState,
                item:payload
            }
        default:
            return{
                ...initialState
            }
    }
}