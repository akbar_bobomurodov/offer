import {item, itemId} from '../constant/index';
export default function(initialState={},{type, payload}){
    switch(type){
        case item.success:
            return{
                ...initialState,
                item:payload
            }
        case itemId.success:
            return{
                ...initialState,
                itemId:payload
            }
        default:
            return{
                ...initialState
            }
    }
}