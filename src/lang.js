export const texts={
    'UZ':{
        add:'qo\'shmoq',
        save:'saqlash',
        cancel:'orqaga',
        category:'bo\'limlar',
        items:'yeguliklar',
        orders:'buyurtmalar',
        search:'qidirish'
    },
    'RU':{
        add:'добавить',
        save:'сохранить',
        cancel:'назад',
        category:'категории',
        items:'предметы',
        orders:'заказы',
        search:'найти'
    }
}