import { item, category, itemId, korzina, lang,deleteItem } from '../constant/index';
export const loadItem = (postsPages, currentPage) => {
  return {
    type: item.success,
    payload: {
      postsPages,
      currentPage
    }
  }
}

export const loadCategory = () => {
  return {
    type: category.success,
    payload: null
  }
}

export const loadItemId = (postsPages, currentPage, id) => {
  return {
    type: itemId.success,
    payload: {
      postsPages,
      currentPage,
      id
    }
  }
}

export const addKorzina = (data) => {
  return {
    type: korzina.success,
    payload: null
  }
}

export const allKorzina = () => {
  return {
    type: korzina.allsuccess,
    payload: null
  }
}

export const addSecondKorzina = (data, div) => {
  return {
    type: korzina.secondsuccess,
    payload: {
      data,
      div
    }
  }
}


export const changeLang = (data) => {
  return {
    type: lang.success,
    payload: data
  }
}

// <--------- DELETE ITEM ---------->

export const deleteItemFunc=()=>{
  return{
    type:deleteItem.success,
    payload:null
  }
}