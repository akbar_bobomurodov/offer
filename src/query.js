import React from 'react';
import QrReader from 'react-qr-scanner'
class Example extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            delay: 100,
            result: null,
        }

        this.handleScan = this.handleScan.bind(this)
    }
    handleScan(data) {
        if(data){
            let tableId=data.split('/');
            this.setState({
                result: tableId[tableId.length-1],
            })
        }else{
            
        }
       if(this.state.result){
           document.cookie=`query=${this.state.result}`
           this.props.history.push('/dashboard')
       }
    }
    handleError(err) {
        console.error(err)
    }
    render() {
        const previewStyle = {
            height: 600,
            width: 1000,
            margin:'auto'
        }
        console.log(this.state.result)
        return (
            <div>
                <QrReader
                    delay={this.state.delay}
                    style={previewStyle}
                    onError={this.handleError}
                    onScan={this.handleScan}
                />
                <p>{this.state.result}</p>
            </div>
        )
    }
}

export default Example;