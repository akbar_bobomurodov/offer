import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Category from './container/category/category';
import Item from './container/item/item';
import CategoryId from './container/category/category_id';
class RoutesNotQuery extends React.Component {

    render() {
        return (
            <div>
                <Router>
                    <Switch>
                        <Route exact path="/dashboard" component={Item} />
                        <Route exact path="/dashboard/category" component={Category} />
                        <Route path="/dashboard/category/:id" component={CategoryId} />
                    </Switch>
                </Router>

            </div>
        )
    }
}

export default RoutesNotQuery;