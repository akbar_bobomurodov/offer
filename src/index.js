import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import Routes from './routes';
import { Provider } from 'react-redux';
import store from './store';
import RoutesNotQuery from './routes_not_query';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isAllow: true
        }
    }
   componentDidMount(){
    let arrayPath=window.location.pathname.split('/');
    if(arrayPath.includes('qrCode')){
        this.setState({
            isAllow:false
        })
    }
   }

    render() {
        return (
            <div>
                {
                    this.state.isAllow ? <Provider store={store}>
                        <Routes />
                    </Provider> : <Provider store={store}>
                            <RoutesNotQuery />
                        </Provider>
                }
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
serviceWorker.unregister();
